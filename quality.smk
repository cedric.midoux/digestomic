rule fastqc:
	input:
		"DATA/raw/{reads}.fastq.gz"
	output:
		zip = "work/fastqc/{reads}_fastqc.zip",
		html = "work/fastqc/{reads}_fastqc.html"
	params:
		output = "work/fastqc/"
	shell:
		"fastqc "
		"{input} "
		"--noextract "
		"--outdir {params.output} "

rule multiqc:
	input:
		expand("work/fastqc/{sample}_{R}_fastqc.zip", sample=SAMPLES, R=["R1", "R2"]),
		expand("work/fastp/{sample}_fastp.json", sample=SAMPLES)
	output:
		html = "report/multiqc_report.html",
	params:
		output = "report/"
	shell:
		"multiqc "
		"--force "
		"--no-data-dir "
		"--outdir {params.output} "
		"{input} "
