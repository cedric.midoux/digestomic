#!/bin/bash
export PYTHONPATH=''
source activate snakemake-5.4.5

mkdir -p ./.log/out/ ./.log/err/

snakemake \
--snakefile $1 \
--jobscript jobscript.sh \
--cluster-config cluster.json \
--cluster "qsub -V -cwd -R y -N {rule} -o {cluster.out} -e {cluster.err} -q {cluster.queue} -pe thread {threads} {cluster.cluster}" \
--keep-going \
--jobs 80 \
--use-conda \
--conda-prefix /projet/maiage/save/cmidoux/environments/ \
--wait-for-files \
--latency-wait 150 \
--use-conda \
--verbose \
--printshellcmds
