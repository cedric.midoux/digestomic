#!/bin/bash
export PYTHONPATH=''
source activate snakemake-5.4.5

snakemake \
--snakefile $1 \
--jobscript jobscript.sh \
--cluster-config cluster.json \
--dryrun \
--forceall \
--printshellcmds \
--verbose
