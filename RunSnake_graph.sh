#!/bin/bash
export PYTHONPATH=''
source activate snakemake-5.4.5

mkdir -p report/

snakemake \
--snakefile $1 \
--dag \
| dot -Tpdf > ./report/`basename $1 .smk`_graph.pdf
