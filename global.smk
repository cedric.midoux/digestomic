import os
configfile: "./config.json"

SAMPLES=config["SAMPLES"]

rule all:
	input:
		"report/multiqc_report.html",
		expand("report/reads_{sample}-krona.html", sample=SAMPLES),
		#expand("report/contigs_{sample}-krona.html", sample=SAMPLES),
		#"report/quast_results/report.html",
		#expand("work/megahit/{sample}/{sample}.contigs.fa", sample=SAMPLES),
		"work/megahit/coassembly/coassembly.contigs.fa",
		"report/quast_coassembly/report.html",
		"work/FGS/coassembly_FGS.gff",
		"report/diamond-NR_coassembly.tsv",
		expand("report/count-contigs-{sample}-coassembly.tsv", sample=SAMPLES),
		expand("report/count-genes_FGS-{sample}-coassembly.tsv", sample=SAMPLES),
		"report/genes_coassembly-taxNames.tsv",
		"report/contigs_coassembly-taxNames.tsv"#,
		#"work/eggnog/coassembly-bact.emapper.hmm_hits"

include: "quality.smk"
include: "preprocess.smk"
include: "kaiju.smk"
include: "assembly.smk"
include: "annotation.smk"
include: "count.smk"
