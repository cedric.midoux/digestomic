#!/bin/bash
export PYTHONPATH=''
source activate snakemake-5.4.5

mkdir -p report/

snakemake \
--snakefile $1 \
--jobscript jobscript.sh \
--cluster-config cluster.json \
--report report/`basename $1 .smk`_report.html \
--verbose
