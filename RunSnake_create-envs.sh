#!/bin/bash
export PYTHONPATH=''
source activate snakemake-5.4.5

snakemake \
--snakefile $1 \
--use-conda \
--conda-prefix /projet/maiage/save/cmidoux/environments/ \
--create-envs-only \
--jobscript jobscript.sh \
--cluster-config cluster.json \
--printshellcmds \
--verbose
